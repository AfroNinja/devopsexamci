package com.agiletestingalliance;
import static org.junit.Assert.*;
import org.junit.Test;

public class MinMaxTest {	
    @Test
    public void testSigkrisi() throws Exception {
        int k1 = new MinMax().sigkrisi(5,4);
        assertEquals("Sigkrisi",5,k1);
		
	int k2 = new MinMax().sigkrisi(4,6);
        assertEquals("Sigkrisi",6,k2);
    }

    @Test
    public void testBar() throws Exception {
        String k1 = new MinMax().bar("alex");
        assertEquals("Bar","alex",k1);
		
	    String k2 = new MinMax().bar("");
        assertEquals("Bar","",k2);
		
		String k3 = new MinMax().bar(null);
        assertEquals("Bar",null,k3);
    }
}
